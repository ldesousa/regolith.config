# regolith.config

My configurations for the Regolith window manager.

The contents of this repository are supposed to go into the folder `~/.config/regolith3`.

Create a symbolic link with:

`ln -s ~/git/regolith.config  ~/.config/regolith3`

## Dependencies

The screen shooting software is not installed by default, use:

`aptitude install scrot`
